#
# Cookbook Name:: opsworks-resque
# Recipe:: default
#
# Copyright (C) 2014 PEDRO AXELRUD
#
# All rights reserved - Do Not Redistribute
#



node[:deploy].each do |application, deploy|

  Chef::Log.info("Configuring resque for application #{application}")

  template "/etc/init/resque-#{application}.conf" do
    source "resque.conf.erb"
    mode '0644'
    variables deploy: deploy
  end

  rack_env = deploy[:rails_env] 

  idx = 1
  template "/etc/init/resque-#{application}-1.conf" do
      source "resque-n.conf.erb"
      mode '0644'
      variables application: application, rack_env: rack_env, deploy: deploy, queue: '*', instance: idx
  end

end